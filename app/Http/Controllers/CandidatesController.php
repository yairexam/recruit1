<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\City;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;


// full name is "App\Http\Controllers\CandidatesController";
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *$user->sortable()->paginate(10);
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::sortable()->get();
        $users = User::all();
        $statuses = Status::all();
        $cities = City::all();
        return view('candidates.index', compact('candidates','users', 'statuses','cities'));
    }

    public function myCandidates()
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $candidates = Candidate::sortable()->where('user_id','=',$userId)->get();
        //$candidates = $user->candidates;
        $users = User::all();
        $statuses = Status::all();
        $cities = City::all();
        return view('candidates.index', compact('candidates','users', 'statuses','cities'));
    }



    public function changeUser($cid, $uid = null){
        Gate::authorize('assign-user');
        $candidate = Candidate::findOrFail($cid);
        $candidate->user_id = $uid;
        $candidate->save();
        return back();
        //return redirect('candidates');
    }

    public function changeStatus($cid, $sid)
    {
        $candidate = Candidate::findOrFail($cid);
        if(Gate::allows('change-status', $candidate))
        {
            $from = $candidate->status->id;
            if(!Status::allowed($from,$sid)) return redirect('candidates');
            $candidate->status_id = $sid;
            $candidate->save();
        }else{
            Session::flash('notallowed', 'You are not allowed to change the status of the user becuase you are not the owner of the user');
        }
        return back();
        //return redirect('candidates');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        return view('candidates.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $candidate = new Candidate();
        $can = $candidate->create($request->all());
        $can->status_id = 1;
        if($can->city_id == null){
            $can->city_id = 1;}
        $can->save();
        return redirect('candidates');

    }
    public function statusC(Request $request, $cid)
    {
        $candidate = Candidate::findOrFail($cid);
        if(Gate::allows('change-status', $candidate))
        {
        $candidate->status_id = $request->status_id;
        $candidate->save();
         return back();
        }
        else{
            Session::flash('notallowed', 'You are not allowed to change the status of the user becuase you are not the owner of the user');
            return back();
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::findOrFail($id);
        $cities = City::all();
        return view('candidates.edit', compact('candidate','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $candidate = Candidate::findOrFail($id);
       $candidate->update($request->all());
       return redirect('candidates');
    }

    public function page($id)
    {
        $candidate = Candidate::findOrFail($id);
        $status = Status::all();
        return view('candidates.page', compact('candidate','status'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id);
        $candidate->delete();
        return redirect('candidates');
    }
}
