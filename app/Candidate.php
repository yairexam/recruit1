<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;


class Candidate extends Model
{
    use Sortable;

    protected $fillable = ['name','email','age','city_id'];
    public $sortable = ['id','name','email','age','user_id','status_id','updated_at','created_at'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }

}
