@extends('layouts.app')

@section('title', 'Personal Page')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif

<h1>Personal Details</h1>
<div><a href =  "{{url('/candidates')}}"> Back to candidates list</a></div>
<table class = "table table-striped">
    <tr>
        <th></th><th>Details</th>
    </tr>
    <!-- the table data -->
    <tr><td>Id</td><td>{{$candidate->id}}</td></tr>
    <tr><td>Name</td><td>{{$candidate->name}}</td></tr>
    <tr><td>Age</td><td>{{$candidate->age}}</td></tr>
    <tr><td>City</td><td>{{$candidate->city->name}}</td></tr>
    <tr><td>Email</td><td>{{$candidate->email}}</td></tr>
    <tr><td>Owner</td><td>@if(isset($candidate->user_id))
            {{$candidate->owner->name}}
          @else
            Assign owner
          @endif</td></tr>
    <tr><td>Status</td><td>
            <div>
                <form method = "post" action = "{{ action('CandidatesController@statusC',$candidate->id) }}">
                    @csrf
                    <div class="form-row align-items-center">
                      <div class="col-auto my-1">
                        @if (App\Status::next($candidate->status_id) != null )
                        <select class="custom-select mr-sm-2" id="status_id" name="status_id">
                            <option value="{{ $candidate->status->id }}">{{$candidate->status->name}}</option>
                            @foreach(App\Status::next($candidate->status_id) as $status)
                          <option value={{$status->id}}>{{$status->name}}</option>
                          @endforeach
                        </select>
                    </div>
                        <div class="col-auto my-1">
                            <button type="submit" name = "submit" class="btn btn-primary">Update status</button>
                          </div>
                        @else
                        {{$candidate->status->name}}
                        @endif
                    </div>
                  </form>
            </td></tr>
    <tr><td>Created</td><td>{{$candidate->created_at}}</td></tr>
    <tr><td>Updated</td><td>{{$candidate->updated_at}}</td></tr>
</table>
@endsection
