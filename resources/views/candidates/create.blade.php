@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')
        <h1>Create candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        @csrf
        <div class="form-group">
            <label for = "name">Candiadte name</label>
            <input type = "text" class="form-control" name = "name">
        </div>
        <div class="form-group">
            <label for = "email">Candiadte email</label>
            <input type = "text" class="form-control" name = "email">
        </div>
        <div class="form-group">
            <label for = "age">Candiadte age</label>
            <input type = "number" class="form-control" name = "age">
        </div>
        <div class="form-group">
            <label for="city_id">City</label>
            <div class="form-group">
                <select class="form-control" name="city_id">
                   @foreach ($cities as $city)
                     <option value="{{ $city->id }}">
                         {{ $city->name }}
                     </option>
                   @endforeach
                 </select>
            </div>
        </div>
        <div>
            <input type = "submit" name = "submit" value = "Create candidate">
        </div>
        </form>
@endsection
