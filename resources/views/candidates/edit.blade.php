@extends('layouts.app')

@section('title', 'Edit candidate')

@section('content')
       <h1>Edit candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "name">Candiadte name</label>
            <input type = "text" class="form-control" name = "name" value = {{$candidate->name}}>
        </div>
        <div class="form-group">
            <label for = "email">Candiadte email</label>
            <input type = "text" class="form-control" name = "email" value = {{$candidate->email}}>
        </div>
        <div class="form-group">
            <label for = "age">Candiadte age</label>
            <input type = "number" class="form-control" name = "age" value = {{$candidate->age}}>
        </div>
        <div class="form-group">
            <label for="city_id">City</label>
            <div class="form-group">
                <select class="form-control" name="city_id">
                <option value="{{ $candidate->city_id }}">{{$candidate->city->name}}</option>
                   @foreach ($cities as $city)
                   @if($city->id!=$candidate->city_id)
                     <option value="{{ $city->id }}">
                         {{ $city->name }}
                     </option>
                     @endif
                   @endforeach
                 </select>
            </div>
        </div>
        <div>
            <input type = "submit" name = "submit" value = "Update candidate">
        </div>
        </form>
    </body>
</html>
@endsection
