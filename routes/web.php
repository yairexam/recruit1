<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('users', 'UsersController')->middleware('auth');

Route::resource('candidates', 'CandidatesController')->middleware('auth');

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser')->middleware('auth');
Route::get('candidates/changestatus/{cid}/{sid?}', 'CandidatesController@changeStatus')->name('candidates.changestatus')->middleware('auth');

Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');

Route::post('candidates/statusC/{cid}', 'CandidatesController@statusC')->name('candidates.statusc')->middleware('auth');
Route::get('candidates/Page/{id}', 'CandidatesController@page')->name('candidate.page')->middleware('auth');

Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete')->middleware('auth');
Route::get('users/delete/{id}', 'UsersController@destroy')->name('user.delete')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
